from django.shortcuts import render
from datetime import datetime

curr_year = datetime.now().strftime("%Y")
def calculate_age(birth_year):
    return int(curr_year) - birth_year
    
mhs_name = 'Debora Dian Pratama Budiharja'
my_age = calculate_age(1999)

def index(request):
    response = {'name': mhs_name, 'age' : my_age}
    return render(request, 'index.html', response)


